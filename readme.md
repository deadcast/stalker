Stalker
=======

A jQuery plugin that allows an element to always stay visible inside a container.
-----------------------------------------------------------------------------

To start using Stalker, just load the plugin like any other jQuery plugin:

	$('#amazing').stalkMe({container : '#container'});
	
That's it! :)

You can also add padding to the top of the element you're following by passing in:

	padding_top : '100' // units are in pixels

Your container element needs to have a relative position in order for the plugin to work correctly.

The test suite uses [Jasmine](https://github.com/pivotal/jasmine) and currently only runs in FireFox. Stalker does however seem to run fine in FF, Chrome, Safari, IE(7,8,9).

Improvements are welcome! :)

Update
------

You no longer need to include the stalker.css file anymore. The css is automatically injected now.