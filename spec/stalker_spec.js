jasmine.getFixtures().fixturesPath = 'spec/fixtures';

describe('#stalkMe', function() {
  describe('#update_element', function() {
    beforeEach(function() {
      loadFixtures('fixture_1.html');
      $('#dress').stalkMe({container : '#image_container'});
    });

    afterEach(function() {
      expect($('#dress')[0].classList.length).toBe(1);
    });

    describe('when a user scrolls the page down', function() {
      it('should move the image', function() {
        $(window).scrollTop($(document).height()/2).trigger('scroll');
        expect($(window).scrollTop()).toEqual($('#dress').offset().top);
        expect($('#dress').hasClass('stalker_fixed')).toBeTruthy();
      });

      it('should not show the image beyond the bottom of the container', function() {
        $(window).scrollTop($(document).height()).trigger('scroll');
        expect($('#image_container').height() + $('#image_container').offset().top)
        .toEqual($('#dress').height() + $('#dress').offset().top);
        expect($('#dress').hasClass('stalker_bottom')).toBeTruthy();
      });

      describe('and there is top padding added', function() {
        beforeEach(function() {
          $('#dress').stalkMe({container : '#image_container', padding_top : '100'});
        });

        it('should move image down with padding', function() {
          $(window).scrollTop($(document).height()/2).trigger('scroll');
          expect($(window).scrollTop()).toEqual($('#dress').offset().top - 100);
          expect($('#dress').hasClass('stalker_fixed')).toBeTruthy();
        });
      });
    });

    describe('when a user scrolls the page up', function() {
      beforeEach(function() {
        $(window).scrollTop($(document).height()).trigger('scroll');
        expect($('#dress').hasClass('stalker_bottom')).toBeTruthy();
      });

      it('should move the image', function() {
        $(window).scrollTop($(document).height()/2).trigger('scroll');
        expect($('#dress').hasClass('stalker_fixed')).toBeTruthy();
        expect($(window).scrollTop()).toEqual($('#dress').offset().top);
      });

      it('should not go beyond the top of the container', function() {
        $(window).scrollTop(0).trigger('scroll').trigger('scroll');
        expect($('#image_container').offset().top).toEqual($('#dress').offset().top);
        expect($('#dress').hasClass('stalker_top')).toBeTruthy();
      });
    });
  });

  describe('validations', function() {
    beforeEach(function() {
      loadFixtures('fixture_1.html');
    });

    describe('when no container is set', function() {
      it('should throw an error', function() {
        expect(function() {
          $('#dress').stalkMe()
          }).toThrow('Stalker requires a container to be set.');
        });
      });

    describe('when an invalid padding_top value is passed', function() {
      it('should throw an error for a negative number', function() {
        expect(function() {
          $('#dress').stalkMe({ container : '#my_div', padding_top : '-10' })
        }).toThrow('Stalker padding_top is invalid. It must be a positive integer.');
      });

      it('should throw an error when NaN', function() {
        expect(function() {
          $('#dress').stalkMe({ container : '#my_div', padding_top : 'cats' })
        }).toThrow('Stalker padding_top is invalid. It must be a positive integer.');
      });
    });
  });
});