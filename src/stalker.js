(function($){
  $.fn.stalkMe = function(options) {  
    return this.each(function() {        
      var $this = $(this), padding_top_int;

      function init() {
        validate_options();

        var padding_top = options.padding_top ? options.padding_top : '0';
        var css = '.stalker_fixed{position:fixed;top:'+ padding_top +'px;}.stalker_top{position:absolute;top:0;}.stalker_bottom{position:absolute;bottom:0;}';
        padding_top_int = parseInt(padding_top);

        var style = document.createElement('style');
        $(style).html(css).appendTo('head');

        update_element();
      }

      function validate_options() {
        if(!options || !options.container)
        throw 'Stalker requires a container to be set.'

        if(options.padding_top) {
          if(isNaN(options.padding_top) || options.padding_top < 0)
          throw 'Stalker padding_top is invalid. It must be a positive integer.'
        }
      }

      function update_element() {
        window_top = $(window).scrollTop();
        container_top = $(options.container).offset().top - padding_top_int;
        container_bottom = $(options.container).height() + container_top;
        image_top = $this.offset().top - padding_top_int;
        image_bottom = $this.height() + image_top;

        if(window_top > image_top && !$this.hasClass('stalker_bottom')) {
          $this.removeClass('stalker_top').addClass('stalker_fixed');
        } else if(container_top > image_top && $this.hasClass('stalker_fixed')) {
          $this.removeClass('stalker_fixed').addClass('stalker_top');
        } else if(image_bottom > container_bottom) {
          $this.removeClass('stalker_fixed').addClass('stalker_bottom');
        } else if(window_top < image_top && $this.hasClass('stalker_bottom')) {
          $this.removeClass('stalker_bottom').addClass('stalker_fixed');
        }
      }

      init();
      $(window).scroll(function() { update_element(); });
    });
  };
})(jQuery);